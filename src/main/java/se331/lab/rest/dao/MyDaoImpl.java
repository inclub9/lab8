package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Profile("MyDao")
@Repository
@Slf4j
public class MyDaoImpl implements StudentDao {
    List<Student> students;

    public MyDaoImpl() {
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(99l)
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .gpa(3.59)
                .image("http://13.250.41.39:8190/images/tu.jpg")
                .penAmount(15)
                .description("The great man ever!!!!")
                .build());
    }

    @Override
    public List<Student> getStudents() {
        log.info("My dao is called");
        return this.students;
    }

    @Override
    public Student getStudent(Long id) {
        return this.students.get(Math.toIntExact(id-1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) this.students.size());
        this.students.add(student);
        return student;
    }
}
